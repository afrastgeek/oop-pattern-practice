import javafx.application.Application;
import javafx.stage.Stage;
import view.View;

public class Main extends Application {
  @Override
  public void start(Stage stage) throws ClassNotFoundException {
    stage.setTitle("No title specification.");
    stage.setScene(new View());
    stage.show();
  }
}
