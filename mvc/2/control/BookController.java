package control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Book;

import java.util.List;

public class BookController {
  public BookController() {
    // konstruktor
  }

  public ObservableList<Book> getAll() {
    ObservableList<Book> books = FXCollections.observableArrayList();
    // data retrieval logic
    return books;
  }

  public void addAll(List<Book> books) {
    // data entry logic
  }

  public void removeAll(List<Book> books) {
    // data removal logic
  }

  public void update(Book book) {
    // data update logic
  }

}
