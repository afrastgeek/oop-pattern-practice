package controller;

import java.io.*;
import model.Telepon;

public class ProsesTelepon{

  private int jml;
  private String kode;
  private String telp;
  private String[][] daftar = new String[25][5];

  private String error;

  public ProsesTelepon(){
    //konstruktor
    this.jml = 0;
  }

  public void prosesTlp() {
    // data retrieval logic
  }

  public int getJml() {
    return this.jml;
  }

  public String[][] getHasil() {
    return this.daftar;
  }

  public String getError() {
    return this.error;
  }

  /*fungsi untuk menambahkan data ke dalam database*/
  public void tambahAnggota(String a, String b) {
    // data entry logic
  }
}
