import javafx.application.Application;
import javafx.stage.Stage;
import view.Table;

public class Main extends Application{
  @Override
  public void start(Stage stage) throws Exception {
    Table tampilbrg = new Table();
    tampilbrg.tampil(stage);
  }
  public static void main(String[] args) {
    launch(args);
  }
}
