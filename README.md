# Memahami GUI Architectural Design Pattern

Sesi ini ditujukan untuk memperkenalkan GUI Architectural Design Pattern dan
implementasinya menggunakan JavaFX.

Secara umum terdapat beberapa pattern yang dapat digunakan, yakni:

- MVC
- MVP
- MVVM
- PMVC

Untuk lebih lengkapnya, silakan buka presentasi di https://gitpitch.com/afrastgeek/oop-pattern-practice?grs=gitlab

Atau baca file [PITCHME.md](PITCHME.md)