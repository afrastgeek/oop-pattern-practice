@title[Introduction]

# GUI Architectural Design Pattern
<br>
Praktikum Pemrograman Berbasis Objek
<br>
Ilmu Komputer, Universitas Pendidikan Indonesia
<br>
 2018

---

Beberapa Pattern

- MVC
- MVP
- MVVM
- PMVC

---

# MVC
Model-View-Controller

![MVC Diagram](http://aalmiray.github.io/griffon-patterns/images/mvc.png)

---

# MVP
Model-View-Presenter

![MVP Diagram](http://aalmiray.github.io/griffon-patterns/images/mvp.png)

---

# MVVM
Model-View-ViewModel

![MVVM Diagram](http://aalmiray.github.io/griffon-patterns/images/mvvm.png)

---

# PMVC
Presentation Model-View-Controller

![PMVC Diagram](http://aalmiray.github.io/griffon-patterns/images/pmvc.png)

---